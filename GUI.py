import sys
import os
from PyQt5.QtWidgets import QApplication, QWidget, QFileDialog, QProgressBar,\
                            QVBoxLayout, QComboBox, QLabel, QLineEdit, QPushButton, QMessageBox
from PyQt5.QtCore import QThread, pyqtSignal, QObject, pyqtSlot
from compress import compressFromGUI, delete
import time

ratioSelected = 0
qualitySelected = 0
folder = ""


class Fenetre(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        qualityInput = ["10" , "20", "30", "40", "50", "60", "70", "80", "90",]
        ratioInput = ["0.1", "0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.9", "1"]

        # Définition folder input
        self.labelFolder = QLabel("Dossier à traiter:")
        self.folder = QLineEdit()

        #Définition bouton d'éxécution
        self.popup = PopUpProgressB()
        self.bouton = QPushButton("COMPRESS")
        #self.bouton.clicked.connect(self.popup.start_progress)
        self.bouton.clicked.connect(self.clickToCompress)

        #Définition bouton d'éxécution
        self.boutonfolder = QPushButton("Choisir un dossier")
        self.boutonfolder.clicked.connect(self.open)
        

        # Qualité conversion
        self.labelQuality = QLabel("Qualité de la conversion (10 à 90%) :")
        self.cbQuality = QComboBox()
        self.cbQuality.addItems(qualityInput)

        # Ratio conversion
        self.labelRatio = QLabel("Ratio de la conversion (0.5 = moitié du ratio) :")
        self.cbRatio = QComboBox()
        self.cbRatio.addItems(ratioInput)

        # Définition layout
        layout = QVBoxLayout()
        # Assignation layout
        layout.addWidget(self.labelFolder)
        layout.addWidget(self.folder)
        layout.addWidget(self.boutonfolder)
        layout.addWidget(self.labelQuality)
        layout.addWidget(self.cbQuality)
        layout.addWidget(self.labelRatio)
        layout.addWidget(self.cbRatio)
        layout.addWidget(self.bouton)
        
        self.setLayout(layout)
        self.setWindowTitle("El compressor")

    def clickToCompress(self):
        folder = self.folder.text()
        ratioSelected = float(self.cbRatio.currentText())
        qualitySelected = int(self.cbQuality.currentText())
        if 0.1 < ratioSelected > 0.9 or 10 < qualitySelected > 90 or folder == "":
            print("erreur")
            self.msg = QMessageBox(parent=self)
            self.msg.setWindowTitle("Erreur")
            self.msg.setText("Une valeur est incorrecte")
            x = self.msg.exec_()
        else:
            if checkPath(folder):
                print("exist")
                self.msgfiles = QMessageBox(parent=self)
                self.msgCompressed = QMessageBox(parent=self)
                
                self.filesInfolder, self.filesCompressed = compressFromGUI(folder=folder, quality=qualitySelected, ratio=ratioSelected, gui=self)
                print("filesInfolder" + str(self.filesInfolder))
                print("filesCompressed" + str(self.filesCompressed))

                self.msgfiles.setWindowTitle("Terminé")
                self.msgfiles.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
                self.msgfiles.setText("Voulez-vous supprimer les anciennes images ?")
                self.msgfiles.buttonClicked.connect(self.popup_clicked)
                x = self.msgfiles.exec_()

                self.msgCompressed.setWindowTitle("Terminé")
                self.msgCompressed.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
                self.msgCompressed.setText("Voulez-vous supprimer les nouvelles images ?")
                self.msgCompressed.buttonClicked.connect(self.popup_clicked_compressed)
                x = self.msgCompressed.exec_()

                self.msg = QMessageBox(parent=self)
                self.msg.setWindowTitle("OK")
                self.msg.setText("Terminé master")
                x = self.msg.exec_()
            else:
                self.msg = QMessageBox(parent=self)
                self.msg.setWindowTitle("Erreur")
                self.msg.setText("Le dossier n'existe pas ou n'est pas accessible")
                x = self.msg.exec_()

    def popup_clicked(self, i):
        print("----------------1--------------------")
        print(self.filesInfolder)
        if("Yes" in i.text()):
            delete(folder=self.folder.text(),listFiles=self.filesInfolder)
    
    def popup_clicked_compressed(self, i):
        print("----------------2--------------------")
        print(self.filesCompressed)
        if("Yes" in i.text()):
            delete(folder=self.folder.text(),listFiles=self.filesCompressed)

    def open(self):
        folderpath = QFileDialog.getExistingDirectory(self, 'Select Folder')
        if folderpath != ('', ''):
            print(folderpath)
            self.folder.setText(folderpath)

class Worker(QObject):
    finished = pyqtSignal()
    intReady = pyqtSignal(int)

    @pyqtSlot()
    def proc_counter(self):  # A slot takes no params
        for i in range(1, 100):
            time.sleep(0.1)
            self.intReady.emit(i)

        self.finished.emit()


class PopUpProgressB(QWidget):

    def __init__(self):
        super().__init__()
        self.pbar = QProgressBar(self)
        self.pbar.setGeometry(30, 40, 500, 75)
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.pbar)
        self.setLayout(self.layout)
        self.setGeometry(300, 300, 550, 100)
        self.setWindowTitle('Progress Bar')
        # self.show()

        self.obj = Worker()
        self.thread = QThread()
        self.obj.intReady.connect(self.on_count_changed)
        self.obj.moveToThread(self.thread)
        self.obj.finished.connect(self.thread.quit)
        self.obj.finished.connect(self.hide)  # To hide the progress bar after the progress is completed
        self.thread.started.connect(self.obj.proc_counter)
        # self.thread.start()  # This was moved to start_progress

    def start_progress(self):  # To restart the progress every time
        self.show()
        self.thread.start()

    def on_count_changed(self, value):
        self.pbar.setValue(value)


def checkPath(path):
    if not os.path.exists(path):
        return False
    else:
        return True


def draw_gui():
    app = QApplication.instance() 
    if not app:
        app = QApplication(sys.argv)
    fen = Fenetre()
    fen.show()

    app.exec_()